## Mybatis Plugin Tutorial
You have to configure following stuff to make it work.

### Setup Spring
Spring and Mybatis are supposed to be configured in IntelliJ, then the Plugin can determine 
that you're using Mybatis.

See `images/spring.png` and `images/mybatis.png` for more details.

### Generate Mapper XML
You should generate Mapper XML from mapper interface first, this can be done with the plugin 
easily. Then the Plugin can determine where generated statement xml can locates.
package com.codesmagic.mybatis.sample.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author yanglin
 */
public interface SampleMapper {

  int selectSomething(@Param("amount") int amount);

  int tryToGenerateStatementXmlForThisMethod();

}
